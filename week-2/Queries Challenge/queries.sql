-- Select All Products
    -- /Selecting every product with its information/
    SELECT * FROM products;

    -- /Selecting only product names/
    SELECT name as Products FROM products;

-- Select All Clients
    -- /Selecting every customer with their information/
    SELECT * FROM customers;

    -- /Selecting only the customer names/
    SELECT first_name, last_name FROM customers;

-- Select all order lines from an order
    SELECT * FROM order_lines WHERE order_id = // <<INSERT VALUE>> //;

-- Select all the products with the profits
    -- Making 'profits' into an extra column made from price * stock
    SELECT id, name, price, stock, SUM(price * stock) as profits FROM products
        GROUP BY id ORDER BY id;

-- Select all orders a product has
    -- Selecting an individual product with its id
    SELECT products.name, products.price, count(order_id) as number_of_orders FROM products
        JOIN order_lines ON products.id = product_id
    GROUP BY products.id HAVING products.id = // <<INSERT VALUE>> //;

    -- Selecting all products with the amount of times they've been ordered
    SELECT products.name, products.price, count(order_id) as number_of_orders FROM products
        JOIN order_lines ON products.id = product_id
    GROUP BY products.id;

-- Select the total sales of X product
    -- Showing product id and the current stock
    SELECT products.id, products.name, sum(order_lines.quantity) as sold, stock FROM products
        JOIN order_lines ON products.id = product_id
    GROUP BY products.id  HAVING products.id = // <<INSERT VALUE>> //;

-- Update a Product price by 1.5 times its value
    UPDATE products SET price = price*1.5 WHERE id = // <<INSERT VALUE>> //

-- Select All the customers who bought an X product
    SELECT DISTINCT first_name, last_name, products.name FROM customers
        JOIN orders ON customers.id = customer_id
        JOIN order_lines ON orders.id = order_id
        JOIN products ON  products.id= product_id
    WHERE products.name='INSERT VALUE';

-- Select All orders between dates X and Y
    -- The date format used was 'YY-MM-DD'
    SELECT * FROM orders WHERE date < 'INSERT DATE' AND date > 'INSERT DATE';

-- Select All products with price greater than 4.5
    SELECT name, price FROM products WHERE price > 4.5 ORDER BY price;

-- Select All the products a Customer has bought
    -- Using the customers ID to search for their purchased products
    SELECT DISTINCT products.name as products FROM customers AS customer
        JOIN orders ON customer.id = customer_id
        JOIN order_lines ON orders.id = order_id
        JOIN products ON  products.id= product_id
    WHERE customer.id= // <<INSERT VALUE >> //;

-- Select the total amount of products a X customer bought between 2 dates
  SELECT customers.id, customers.first_name, SUM(order_lines.quantity) FROM customers
        JOIN orders ON customers.id = customer_id
        JOIN order_lines ON order_lines.order_id = orders.id
    WHERE orders.date > 'INSERT DATE' AND orders.date < 'INSERT DATE' AND customers.id = // <<INSERT VALUE >> //
    GROUP BY customers.id;

-- Select what is the most purchased product
    SELECT product_id, SUM(quantity) AS times_bought FROM order_lines
        GROUP BY product_id ORDER BY times_bought DESC LIMIT 1;

-- Delete an specific order
    BEGIN;
    DELETE FROM order_lines WHERE order_id = // <<INSERT VALUE >> //;
    DELETE FROM orders WHERE id = // <<INSERT VALUE >> //;
    COMMIT;
