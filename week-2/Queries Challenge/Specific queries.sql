-- This file contains queries that do a bit more than what was asked


-- Update a Product price by 1.5 times its value
    -- Taking into account the tables with relation to the price value
    BEGIN;
    UPDATE products SET price = ROUND(price*1.5, 2) WHERE id = // <<INSERT VALUE>> //;
    UPDATE order_lines SET price = ROUND (price*1.5, 2),
                    total = ROUND(price*quantity*1.5, 2)
        WHERE product_id = // <<INSERT VALUE>> //;
    UPDATE orders SET total =
        (SELECT SUM(total) FROM order_lines
        WHERE orders.id= order_lines.order_id
        GROUP BY order_id)
    WHERE orders.id IN (SELECT order_id FROM order_lines WHERE product_id= <<INSERT VALUE>>);
    COMMIT;

-- Select the total sales of X product
    -- Without showing product id
    SELECT products.name, sum(order_lines.quantity) as sold, stock FROM products
        JOIN order_lines ON products.id = product_id
    GROUP BY products.id  HAVING products.id = // <<INSERT VALUE>> //;

      --Show all product sales orders showing just product id
    --//**Change ORDER BY products.id into ORDER BY sold to see from most to least sold**//
    SELECT products.name, sum(order_lines.quantity) as sold, stock FROM products
        JOIN order_lines ON products.id = product_id
    GROUP BY products.id ORDER BY products.id;


-- Select the total amount of products a X customer bought between 2 dates
    --Using a different way to compare dates
  SELECT customers.id, customers.first_name, SUM(order_lines.quantity) FROM customers
        JOIN orders ON customers.id = customer_id
        JOIN order_lines ON order_lines.order_id = orders.id
    WHERE '[INSERT START DATE, INSERT END DATE]'::daterange @> orders.date AND customers.id = // <<INSERT VALUE >> //
    GROUP BY customers.id;
