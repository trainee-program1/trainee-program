require './vehicle_class'

class Truck < Vehicle
  attr_reader :load_capacity, :axle_num

  def initialize(wheel_num, color, brand, price, axle_num, load_capacity)
    super(wheel_num, color, brand, price)
    @axle_num = axle_num
    @load_capacity = load_capacity # Assumes capacity's unit is lbs
  end

  def display
    super.concat(
      "\tAxle Number       : #{axle_num}\n\n",
      "\tLoad capacity     : #{load_capacity}\n"
    )
  end

  def self.create(amount = 1)
    super amount do |wheels, col, manufacturer, price|
      ax_num = rand(2..8)
      capacity = rand(18.5..40).round(2) * 1000
      Truck.new(wheels, col, manufacturer, price, ax_num, capacity)
    end
  end
end
