class Extras
  @@defaults = [['Radio', 49.99], ['AC', 114.99], ['Sunroof', 149.99], ['Leather seats', 1500], ['Power windows', 300]]

  def initialize(extras = @@defaults)
    # Use an array of arrays containing name and price to add custom extras.
    # Or an index, which will be used to get one of the @@defaults
    @features = {}

    extras = [@@defaults.fetch(extras - 1)] if extras.is_a? Integer

    extras.each { |name, price| features[name.downcase] = price }
  end

  def add_to_vehicle(vehicle)
    vehicle.add_feature(features)
  end

  attr_accessor :features

  def display
    "Extras:\n" << features.map { |name, price| "\t#{name.capitalize} (#{price})\n" }.join("\n")
  end
end
