require 'faker'
require './extra_class'
require 'securerandom'

class Vehicle
  attr_reader :wheel_num, :brand, :extras, :id # Restricting writing permissions
  attr_accessor :color, :price, :plates

  def initialize(wheel_num, color, brand, price)
    @wheel_num = wheel_num
    @color = color
    @brand = brand
    @price = price
    @id = SecureRandom.uuid
    @extras = Extras.new []
  end

  def add_feature(extra)
    extra.each { |name, price| @extras.features[name] = price }
  end

  def remove_feature(extra)
    @extras.features.delete extra
  end

  def display
    "\tNumber of wheels    : #{wheel_num}\n\n".concat(
      "\tColor             : #{color.capitalize}\n\n",
      "\tBrand             : #{brand.capitalize}\n\n",
      "\tPrice             : #{price} $\n\n"
    )
  end

  def total
    total = 0
    @extras.features.each { |name, value| total += value } if @extras.features.size.positive?
    total + price
  end

  def self.create(amount = 1)
    # Creates random vehicle instances that will be used to initialize child classes
    created = (1..amount).map do
      wheels = rand(4..8)
      wheels = wheels.even? ? wheels : wheels + 1
      color = Faker::Color.color_name
      brand = Faker::Vehicle.make
      price = Faker::Commerce.price(range: 5000..100_000)

      vehicle = yield wheels, color, brand, price if block_given?
      vehicle
    end
    return created[0] if amount == 1
    created
  end
end
