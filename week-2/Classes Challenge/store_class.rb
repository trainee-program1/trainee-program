require './extra_class'
require './car_class'
require './truck_class'

class Store
  attr_reader :vehicles

  def initialize(vehicles = [])
    @vehicles = {}
    vehicles.each { |vehicle| add_vehicle(vehicle) }
  end

  def display_all
    # Lists all vehicles
    vehicles.map.with_index do |vehicle, index|
      line = "#{'-' * 30} #{index+1} #{'-' * 30}\n"
      line.concat "#{vehicle[1].class}:\n\n",
                  "\tVehicle id        : #{vehicle[1].id}\n\n",
                  vehicle[1].display,
                  line
    end
  end

  def add_vehicle(vehicle)
    return @vehicles[vehicle.id] = vehicle unless @vehicles[vehicle.id]

    puts 'Car id cannot be repeated'
  end

  def remove_vehicle(id)
    @vehicles.delete id
  end

  def print_invoice(vehicle, extras)
    line = "#{'-' * 40}\n\n"
    "\n\nDetail:\n\n\n".concat(
      "\tQuote for the car: #{vehicle.id}\n\n",
      vehicle.display,
      line, extras.display,
      line, "\tTotal: #{vehicle.total}"
    )
  end

  def find_by_id (id)
    @vehicles[id]
  end

  def self.create
    vehicles = Car.create(5).map { |cars| cars }
    Truck.create(5).each { |trucks| vehicles << trucks}
    Store.new(vehicles)
  end
end
